<?php
/**
 * Created by PhpStorm.
 * User: mr_dreek
 * Date: 15.02.19
 * Time: 14:08
 */

namespace MrDreek\b24rest\Test;

use MrDreek\b24rest\B24rest;

class BTest extends TestCase
{
    /**
     * Проверка получения списка сделок по установленному в конфиге url
     * @throws \Exception
     */
    public function testGetDeals(): void
    {
        $deals = (array)(new B24rest)->getDeals();
        $this->assertArrayHasKey('result', $deals, 'Ответ получен');
    }

    /**
     * Проверка получения информации по конкретной сделке по id
     * @throws \Exception
     */
    public function testGetDeal(): void
    {
        $deals = (array)(new B24rest)->getDeal(1);
        $this->assertArrayHasKey('result', $deals, 'Ответ получен');
    }

    /**
     * Проверка получения информации по конкретной сделке по id
     * @throws \Exception
     */
    public function testGetStorageList(): void
    {
        $deals = (array)(new B24rest)->getStorageList();
        $this->assertArrayHasKey('result', $deals, 'Ответ получен');
    }
}
