<?php
/**
 * Created by PhpStorm.
 * User: mr_dreek
 * Date: 15.02.19
 * Time: 13:58
 */

namespace MrDreek\b24rest;

use Illuminate\Support\ServiceProvider;

class B24restServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/config/b24rest.php' => config_path('b24rest.php'),
        ], 'b24rest');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // Config
        $this->mergeConfigFrom(__DIR__ . '/config/b24rest.php', 'b24rest');

        $this->app->singleton(B24rest::class, function () {
            return new B24rest();
        });
        $this->app->alias(B24rest::class, 'b24rest');
    }
}
