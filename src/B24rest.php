<?php
/**
 * Created by PhpStorm.
 * User: mr_dreek
 * Date: 15.02.19
 * Time: 13:58
 */

namespace MrDreek\b24rest;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class B24rest
{
    private $url;

    private $curl;

    private const GET_DEAL_LIST = 'crm.deal.list.json';
    private const GET_DEAL = 'crm.deal.get.json';
    private const UPDATE_DEAL = 'crm.deal.update.json';
    private const GET_STORAGE_LIST = 'disk.storage.getlist.json';
    private const CREATE_SUBFOLDER = 'disk.folder.addsubfolder.json';
    private const REMOVE_SUBFOLDER = 'disk.folder.deletetree.json';
    private const UPLOAD_FILE = 'disk.folder.uploadfile.json';

    /**
     * B24rest constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $url = config('b24rest.url');

        if ($url === null) {
            throw new \Exception('Invalid configuration exception');
        }
        $this->url = $url;

        $this->curl = new \Ixudra\Curl\CurlService;
    }

    /**
     * @param      $url
     *
     * @param bool $externalUrl
     *
     * @return \Ixudra\Curl\Builder
     */
    private function to($url, $externalUrl = false): \Ixudra\Curl\Builder
    {
        if (!$externalUrl) {
            $response = $this->curl->to($this->url . $url);
        } else {
            $response = $this->curl->to($url);
        }

        if (config('app.proxy')) {
            $response = $response->withProxy(config('app.proxy_url'), config('app.proxy_port'), config('app.proxy_type'), config('app.proxy_username'), config('app.proxy_password'));
        }

        return $response;
    }

    /**
     * Метод получения списка сделок по bitrix rest url
     * @return mixed
     */
    public function getDeals()
    {
        return $this->to(self::GET_DEAL_LIST)
            ->asJson()
            ->get();
    }

    /**
     * Информация по конкретной сделке по id
     *
     * @param $id
     *
     * @return mixed
     */
    public function getDeal($id)
    {
        if ($id === null) {
            throw new \InvalidArgumentException('id is null');
        }

        return $this->to(self::GET_DEAL)
            ->withData([
                'id' => $id,
            ])
            ->asJson()
            ->post();
    }

    /**
     * Информация по доступных хранилищам
     * @return mixed
     */
    public function getStorageList()
    {
        return $this->to(self::GET_STORAGE_LIST)
            ->asJson()
            ->get();
    }

    /**
     * @param $folderId
     * @param $name
     *
     * @return mixed
     */
    public function createSubFolder($folderId, $name)
    {
        if ($name === null || $folderId === null) {
            throw new \InvalidArgumentException('id is null or name is name');
        }

        return $this->to(self::CREATE_SUBFOLDER)
            ->withData([
                'id' => $folderId,
                'data' => [
                    'NAME' => $name,
                ],
            ])
            ->asJson()
            ->post();
    }

    public function deleteFolder($id)
    {
        if ($id === null) {
            throw new \InvalidArgumentException('id is null');
        }

        return $this->to(self::REMOVE_SUBFOLDER)
            ->withData([
                'id' => $id,
            ])
            ->asJson()
            ->post();
    }

    public function uploadFile($folderId, $filename, $filepath)
    {
        if ($folderId === null) {
            throw new \InvalidArgumentException('folderId is null');
        }

        if ($filename === null) {
            throw new \InvalidArgumentException('filename is null');
        }

        if (!is_file($filepath)) {
            throw new \InvalidArgumentException('filename is not path to file');
        }

        $response = $this->to(self::UPLOAD_FILE)
            ->withData([
                'id' => $folderId,
                'data' => $filename,
            ])
            ->asJson()
            ->post();

        if (isset($response->result)) {
            $test = json_decode($this->to($response->result->uploadUrl, true)
                ->withContentType('multipart/form-data')
                ->withFile($response->result->field, $filepath)
                ->post());

            return $test;
        }

        throw new BadRequestHttpException('bad request from bitrix ' . json_encode($response));
    }

    /**
     * @param $dealId
     * @param $fields
     * @param $params
     *
     * @return mixed
     */
    public function updateDeal($dealId, $fields, $params)
    {
        $test = $this->to(self::UPDATE_DEAL)
            ->withData([
                'id' => $dealId,
                'fields' => $fields,
                'params' => $params,
            ])
            ->asJson()
            ->post();

        return $test;
    }
}
