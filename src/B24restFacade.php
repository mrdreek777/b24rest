<?php
/**
 * Created by PhpStorm.
 * User: mr_dreek
 * Date: 15.02.19
 * Time: 14:01
 */

namespace MrDreek\b24rest;

use Illuminate\Support\Facades\Facade;

class B24restFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'B24rest';
    }
}
